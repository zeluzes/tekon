<!-- html inicial -->
<?php  include 'html_inicial.php'; ?>

<div class="owl-carousel owl-theme owl-home">
    <div class="item">
        <img src="assets/img/banner_home.png" alt="banner_home.png">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-lg-offset-1">
                    <div class="wrapper-text">
                        <p>Find out our new wireless Monitoring System DUOS TEMP and HYGROTEMP</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="item">
        <img src="assets/img/banner_home.png" alt="banner_home.png">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-lg-offset-1">
                    <div class="wrapper-text">
                        <p>Find out our new wireless Monitoring System DUOS TEMP and HYGROTEMP</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <img src="assets/img/banner_home.png" alt="banner_home.png">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-lg-offset-1">
                    <div class="wrapper-text">
                        <p>Find out our new wireless Monitoring System DUOS TEMP and HYGROTEMP</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- html final -->
<?php  include 'html_final.php'; ?>