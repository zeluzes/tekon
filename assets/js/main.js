//Chamar todas as funções relacionadas com tamanhos para tomarem alterações no resize
function resizableElements(viewport) {

}

//Chamar todas as funções iniciais, esperando pelas imagens serem carregadas
(function ($, viewport) {
  $(document).ready(function () {
    $('body').waitForImages(function () {
      resizableElements(viewport);

      onInit();
      caroussels();
    });
  });
})(jQuery, ResponsiveBootstrapToolkit);

//Vai chamar a função que possui todas as funções associadas ao resize
(function ($, viewport) {
  $(window).resize(function () {
    resizableElements(viewport);
    onInit();
  });
})(jQuery, ResponsiveBootstrapToolkit);

//Caso se pretenda aplicar um fadeOut ao body antes de todo se carregar
$(window).load(function () {
  $('body').waitForImages(function () {

  });
});

function onInit(){
  $(".main-content").css({"margin-top":$("nav").outerHeight(true)});
}

function caroussels(){

  let owlhome = $(".owl-home");

  owlhome.owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    navText: '',
    dots: false,
  });
}