<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>HTML Starter</title>
    <meta name="description" content="">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1, user-scalable=0" name="viewport">
    <meta content="width" name="MobileOptimized">
    <meta content="true" name="HandheldFriendly">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/vendor.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- endinject -->
</head>

<body>
    <nav>
        <div class="nav_topbar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav_topbar__links">
                            <li class="nav_topbar__links-link">
                                <a href="#" title="become distributor">
                                    <i class="icon-new_user_hover"></i>Become a distributor</a>
                            </li>
                            <li class="nav_topbar__links-link">
                                <a href="#" title="download center">
                                    <i class="icon-technical_support_hover"></i>download center</a>
                            </li>
                            <li class="nav_topbar__links-link">
                                <a href="#" title="Reserved Area">
                                    <i class="icon-reserved_area_on_mobile-01"></i>Reserved Area</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav_mainbar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="nav_mainbar_items">
                            <a href="#" title="tekon" id="logo-link">
                                <img src="assets/img/logo_big.png" alt="logo_big.png">
                            </a>

                            <div class="nav_mainbar__content">
                            <div class="nav_search">
                                    <a href="#" title=""><i class="icon-serach_on"></i></a>
                                </div>
                                <div class="nav_languages">
                                    <a href="#" title=""><img width="17" height="11" src="assets/img/england.png" alt="england.png"></a>
                                </div>
                                <ul class="nav_mainbar__links">
                                    <li class="mainbar__links_link">
                                        <a href="#" title="products">Products</a>
                                    </li>
                                    <li class="mainbar__links_link">
                                        <a href="#" title="products">Products</a>
                                    </li>
                                    <li class="mainbar__links_link">
                                        <a href="#" title="products">Products</a>
                                    </li>
                                </ul>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div class="main-content">